<?php

/**
 * @file
 *   Translation reset administration.
 */

/**
 * Page callback: Reset admin page.
 */
function l10n_update_reset_admin() {
  $translation_count = array();

  if (variable_get('l10n_update_import_mode', LOCALE_IMPORT_KEEP) == LOCALE_IMPORT_KEEP) {
    drupal_set_message(t('Your translation update mode does not allow translations to be deleted. <a href="!url">Change this setting</a> before you continue.', array('!url' => url('admin/config/regional/language/update'))), 'warning');
  }
  
  // Get number of translations per language.
  $translations = db_query("SELECT COUNT(*) AS translation, t.language FROM {locales_target} t GROUP BY language");
  foreach ($translations as $data) {
    $translation_count[$data->language] = $data->translation;
  }

  $languages = l10n_update_language_list('name');
  foreach ($languages as $langcode => $name) {
    $count = isset($translation_count[$langcode]) ? $translation_count[$langcode] : 0;
    $languages[$langcode] = t('!language (@count translations)', array('!language' => $name, '@count' => $count));
  }
  $form['languages'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Languages'),
    '#required' => TRUE,
    '#options' => $languages,
    //'#default_value' => drupal_map_assoc(array_keys($languages)),
  );  
  $form['buttons']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Delete translations'),
  );

  return $form;
}

/**
 * @todo
 */
function l10n_update_reset_admin_submit($form, &$form_state) {
  $languages = array_filter($form_state['values']['languages']);
  $form_state['redirect'] = 'admin/config/regional/translate/reset/delete/' . implode('+', $languages);
}

/**
 * Menu callback: Delete translations
 */
function l10n_update_reset_delete_translations($languages) {
  // @todo filter valid languages.
  $languages = explode('+', $languages);
  $languages = array_intersect($languages, array_keys(l10n_update_language_list()));
  if (empty($languages)) {
    drupal_set_message(t('Todo ... no languages found ...'), 'error');
    drupal_goto('admin/config/regional/translate/reset');
  }
  return drupal_get_form('l10n_update_reset_confirm_delete', $languages);
}

/**
 * @todo
 */
function l10n_update_reset_confirm_delete($form, &$form_state, $languages) {
  $form['#languages'] = $languages;
  $language_names = array_intersect_key(l10n_update_language_list('name'), drupal_map_assoc($languages));
  return confirm_form(
    $form,
    t('Are you sure you want to delete the translations'),
    'admin/config/regional/translate/reset',
    t('Interface translations of the following langague(s) will be deleted:') . '<br />' .
    theme('item_list', array('items' => $language_names)) . '<br />' .
    t('This action cannot be undone.'), 
    t('Delete'));
}

/**
 * @todo
 */
function l10n_update_reset_confirm_delete_submit($form, &$form_state) {
  $languages = $form['#languages'];
  l10n_update_reset_flush_translations($languages);
  l10n_update_reset_reset_l10n_status();
  drupal_set_message(t('Todo ... translations deleted.'));
  $form_state['redirect'] = 'admin/config/regional/translate/reset';
}

/**
 * @todo
 */
function l10n_update_reset_flush_translations($languages) {
  $import_mode = variable_get('l10n_update_import_mode', LOCALE_IMPORT_KEEP);
  foreach ($languages as $language) {
    if ($import_mode != LOCALE_IMPORT_KEEP) {
      if ($import_mode == LOCALE_UPDATE_OVERRIDE_DEFAULT) {
        // Delete translations except those that where locally changed.
        $override = db_select('locales_source', 'ls')
          ->fields('ls', array('lid'))
          ->condition('l10n_status', 1)
          ->condition('language', $language)
          ->where('ls.lid = locales_target.lid');
        db_delete('locales_target')
          ->notExists($override)
          ->condition('language', $language)
          ->execute();
      }
      else {
        // Delete all translations.
        db_delete('locales_target')
          ->condition('language', $language)
          ->execute();
      }
    }
  }

  // Delete all source strings that do not have a translation target.
  $target = db_select('locales_target', 'lt')
    ->distinct()
    ->fields('lt', array('lid'))
    ->where('locales_source.lid = lt.lid');
  db_delete('locales_source')
    ->notExists($target)
    ->execute();
}

/**
 * @todo
 */
function l10n_update_reset_reset_l10n_status() {
  db_delete('l10n_update_file')->execute();
  cache_clear_all('*', 'cache_l10n_update', TRUE);
}

/**
 * @todo
 */
function _l10n_update_reset_get_language_args() {
  if (isset($_GET['lang'])) {
    $lang = check_url($_GET['lang']);
    return explode('+', $lang);
  }
  return array();
}